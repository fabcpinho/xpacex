package com.fpapps.xpacex.launches.api;

import com.fpapps.xpacex.launches.model.Launch;
import io.reactivex.Single;
import java.util.List;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LaunchesApiContract {
  @GET("launches")
  Single<Response<List<Launch>>> getLaunchesByRocket(@Query("rocket_id") String id);
}
