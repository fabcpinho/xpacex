package com.fpapps.xpacex.launches.interactor;

import com.fpapps.xpacex.launches.model.Launch;
import com.fpapps.xpacex.launches.presenter.contract.RocketDetailPresenterContract;
import com.fpapps.xpacex.rockets.repository.RocketsRepository;
import com.fpapps.xpacex.rxjava.RxSchedulers;
import com.jjoe64.graphview.series.DataPoint;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import retrofit2.Response;

public class LaunchesInteractor {
  private final RxSchedulers rxSchedulers;
  private RocketDetailPresenterContract rocketDetailPresenterContract;
  private RocketsRepository rocketsRepository;

  private final CompositeDisposable mDisposables;

  public LaunchesInteractor(RxSchedulers rxSchedulers,
                            RocketDetailPresenterContract rocketDetailPresenterContract,
                            RocketsRepository rocketsRepository) {
    this.rxSchedulers = rxSchedulers;
    this.rocketDetailPresenterContract = rocketDetailPresenterContract;
    this.rocketsRepository = rocketsRepository;

    mDisposables = new CompositeDisposable();
  }

  // TODO: 09/10/2018 This conversions could be optimized with a few more time
  /**
   * Gets the number of launches per year
   * @param launchesList all the launches to analyze
   * @return Array of DataPoints with the final points to display
   */
  private DataPoint[] prepareDataPoints(List<Launch> launchesList) {
    LinkedHashMap<Integer, Integer> launchesPerYear = new LinkedHashMap<>();

    for (Launch l : launchesList) {
      int count = 1;
      if(launchesPerYear.get(l.launchYear) != null){
        count = launchesPerYear.get(l.launchYear) +1;
      }
      launchesPerYear.put(l.launchYear, count);
    }

    return convertHMToDataPoint(launchesPerYear);
  }

  /**
   * This method converts the HashMap into DataPoints needed to display the points in the Graph
   * Basiaclly this method runs through the hashmap and converts each item into a DataPoint
   *
   * @param launchesPerYear hash map with the information to represent
   * @return DataPoint[] with the final points to display
   */
  private DataPoint[] convertHMToDataPoint(LinkedHashMap<Integer, Integer> launchesPerYear) {
    DataPoint[] dataPoints = new DataPoint[launchesPerYear.size()];
    int dataPointSize = 0;

    Iterator it = launchesPerYear.entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry pair = (Map.Entry) it.next();
      int x = (int) pair.getKey();
      int y;
      if (pair.getValue() != null) {
        y = (int) pair.getValue();
      } else {
        y = 0;
      }
      DataPoint dp = new DataPoint(x, y);

      dataPoints[dataPointSize] = dp;
      dataPointSize++;

      it.remove();
    }
    return dataPoints;
  }

  /**
   * If has launches in cache, return them.
   * If not, tries database.
   * If database has no launches, fetch them from api.
   * @param rocketId rocket id for which we want the details
   */
  // TODO: 09/10/2018 Corner case identified: when we have 0 launches for one rocket and we don't have internet connection, app will send a network error instead
  private void getLaunchesOfRocket(String rocketId) {
    mDisposables.add(rocketsRepository.getLaunchesByRocket(rocketId)
        .compose(rxSchedulers.applySingleSchedulerTransformer())
        .subscribe(launches -> {
          if (launches.size() == 0) {
            fetchRocketDetailsFromNetwork(rocketId);
          } else {
            rocketDetailPresenterContract.details(launches, prepareDataPoints(launches));
          }
        }, throwable -> rocketDetailPresenterContract.failedFetchingLaunches(RocketsRepository.ERROR_LOADING_FROM_DATABASE)));
  }

  private void fetchRocketDetailsFromNetwork(String id) {
    mDisposables.add(fetchRocketDetailsFromApi(id).subscribe(launches -> {
      updateLaunchesInDatabase(launches);

      rocketDetailPresenterContract.details(launches, prepareDataPoints(launches));
    }, throwable -> rocketDetailPresenterContract.failedFetchingLaunches(RocketsRepository.ERROR_LOADING_FROM_NETWORK)));
  }

  private Maybe<List<Launch>> fetchRocketDetailsFromApi(String id) {
    return rocketsRepository.getRocketLaunchInfo(id)
        .map(Response::body)
        .filter(launches -> launches != null)
        .compose(rxSchedulers.applyMaybeSchedulerTransformer());
  }

  /**
   * Updates the database with a list of launches
   * @param launchesList launches to insert on DB
   */
  private void updateLaunchesInDatabase(List<Launch> launchesList) {
    mDisposables.add(Single.just(launchesList)
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .subscribe(request -> rocketsRepository.insertLaunchesInDatabase(launchesList)));
  }

  /**
   * Gets rocket detailed information: Launches
   * @param rocketId rocket id for which we want the details
   */
  public void getRocketDetailedInformation(String rocketId) {
    getLaunchesOfRocket(rocketId);
  }

  /**
   * Dispose all observables
   */
  public void dispose(){
    mDisposables.dispose();
  }
}
