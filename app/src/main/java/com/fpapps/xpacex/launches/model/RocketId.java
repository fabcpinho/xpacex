package com.fpapps.xpacex.launches.model;

import android.arch.persistence.room.ColumnInfo;
import android.support.annotation.NonNull;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RocketId {
  @JsonProperty("rocket_id")
  @ColumnInfo(name = "id")
  @NonNull
  public String id;
}
