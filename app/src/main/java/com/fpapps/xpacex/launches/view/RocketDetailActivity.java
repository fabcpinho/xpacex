package com.fpapps.xpacex.launches.view;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.fpapps.xpacex.R;
import com.fpapps.xpacex.dagger.component.DaggerRocketDetailActivityComponent;
import com.fpapps.xpacex.dagger.module.ApplicationModule;
import com.fpapps.xpacex.dagger.module.RocketDetailPresenterModule;
import com.fpapps.xpacex.launches.adapters.LaunchesRecyclerViewAdapter;
import com.fpapps.xpacex.launches.model.Launch;
import com.fpapps.xpacex.launches.presenter.contract.RocketDetailPresenterContract;
import com.fpapps.xpacex.launches.view.contract.RocketDetailActivityContract;
import com.fpapps.xpacex.rockets.repository.RocketsRepository;
import com.fpapps.xpacex.stickyheader.StickyHeaderDecoration;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class RocketDetailActivity extends AppCompatActivity
    implements RocketDetailActivityContract {
  public final static String ROCKET_ID_BUNDLE = "rocket_id_bundle";
  public final static String ROCKET_DESCRIPTION_BUNDLE = "rocket_desc_bundle";
  @Inject
  RocketDetailPresenterContract rocketDetailPresenter;
  private Toolbar toolbar;
  private String rocketId;
  private String description;
  private TextView descriptionTv;
  private RecyclerView mRecyclerView;
  private LaunchesRecyclerViewAdapter mAdapter;
  private GraphView graphView;
  private ProgressBar progressBar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_rocket_detail);

    toolbar = findViewById(R.id.toolbar);
    descriptionTv = findViewById(R.id.description);
    graphView = findViewById(R.id.graph);
    progressBar = findViewById(R.id.pb_loading);

    setSupportActionBar(toolbar);
    toolbar.inflateMenu(R.menu.toolbar_menu);

    handleDaggerInjection();

    setUpRecyclerView();

    rocketId = getIntent().getStringExtra(ROCKET_ID_BUNDLE);
    description = getIntent().getStringExtra(ROCKET_DESCRIPTION_BUNDLE);

    getLaunchesInfo();

    descriptionTv.setText(description);
  }

  private void getLaunchesInfo() {
    progressBar.setIndeterminate(true);
    rocketDetailPresenter.getRocketDetailedInformation(rocketId);
  }

  private void setupGraph(DataPoint[] data) {
    LineGraphSeries<DataPoint> series = new LineGraphSeries<>(data);
    series.setAnimated(true);
    graphView.addSeries(series);
  }

  private void setUpRecyclerView() {
    mRecyclerView = findViewById(R.id.rv_launches);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
    mRecyclerView.setLayoutManager(mLayoutManager);
    mAdapter = new LaunchesRecyclerViewAdapter(new ArrayList<>());

    mRecyclerView.post(() -> {
      mRecyclerView.addItemDecoration(new StickyHeaderDecoration(mAdapter));
    });

    mRecyclerView.setAdapter(mAdapter);
  }

  private void handleDaggerInjection() {
    DaggerRocketDetailActivityComponent.builder()
        .applicationModule(new ApplicationModule(this))
        .rocketDetailPresenterModule(new RocketDetailPresenterModule(this))
        .build()
        .inject(this);
  }

  private void hideProgressBar() {
    progressBar.setIndeterminate(false);
    progressBar.setVisibility(View.GONE);
  }

  @Override
  public void displayListOfLaunches(List<Launch> launches) {
    mAdapter.setData(launches);
    mAdapter.notifyDataSetChanged();
    hideProgressBar();
  }

  @Override
  public void displayGraph(DataPoint[] data) {
    graphView.setVisibility(View.VISIBLE);
    setupGraph(data);
    hideProgressBar();
  }

  @Override
  public void showError(int errorCode) {
    hideProgressBar();

    if (errorCode == RocketsRepository.ERROR_LOADING_FROM_NETWORK) {

      new AlertDialog.Builder(this).setTitle(R.string.no_internet_tittle)
          .setMessage(R.string.no_internet_message)
          .setCancelable(false)
          .setPositiveButton(getString(R.string.try_again), (dialogInterface, i) -> {
            getLaunchesInfo();
            dialogInterface.dismiss();
          })
          .setNegativeButton(getString(R.string.exit), ((dialogInterface, i) -> finish()))
          .show();
    }
  }

  @Override
  public void showEmptyListPlaceholder() {
    mRecyclerView.setVisibility(View.GONE);
    TextView tv = findViewById(R.id.rv_placeholder);
    tv.setVisibility(View.VISIBLE);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    rocketDetailPresenter.dispose();
  }
}
