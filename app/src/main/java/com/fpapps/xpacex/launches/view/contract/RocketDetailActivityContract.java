package com.fpapps.xpacex.launches.view.contract;

import com.fpapps.xpacex.launches.model.Launch;
import com.jjoe64.graphview.series.DataPoint;
import java.util.List;

public interface RocketDetailActivityContract {
  /**
   * Displays the list of launches in the recycler view
   * @param launches to display
   */
  void displayListOfLaunches(List<Launch> launches);

  /**
   * Displays the graph points
   * @param data data to display
   */
  void displayGraph(DataPoint[] data);

  /**
   * Show error dialog
   * @param errorCode might be network or database problem
   */
  void showError(int errorCode);

  /**
   * Show placeholder when have 0 launches to display
   */
  void showEmptyListPlaceholder();
}
