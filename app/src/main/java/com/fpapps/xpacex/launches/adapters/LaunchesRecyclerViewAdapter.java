package com.fpapps.xpacex.launches.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.fpapps.xpacex.R;
import com.fpapps.xpacex.launches.model.Launch;
import com.fpapps.xpacex.stickyheader.StickyHeaderAdapter;
import com.squareup.picasso.Picasso;
import java.util.List;
import jp.wasabeef.picasso.transformations.CropSquareTransformation;

public class LaunchesRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    implements StickyHeaderAdapter {
  private List<Launch> mLaunches;

  public LaunchesRecyclerViewAdapter(List<Launch> mLaunches) {
    this.mLaunches = mLaunches;
  }

  public void setData(List<Launch> mLaunches) {
    this.mLaunches = mLaunches;
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View v = inflater.inflate(R.layout.rv_launch, parent, false);
    return new LaunchViewHolder(v);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    Launch launch = mLaunches.get(position);
    LaunchViewHolder launchViewHolder = (LaunchViewHolder) holder;
    launchViewHolder.setData(launch);
  }

  @Override
  public int getItemCount() {
    return mLaunches != null ? mLaunches.size() : 0;
  }

  @Override
  public long getHeaderId(int position) {
    Launch currentLaunch = mLaunches.get(position);
    return currentLaunch.launchYear;
  }

  @Override
  public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int position) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View v = inflater.inflate(R.layout.rv_header, parent, false);
    return new HeaderViewHolder(v);
  }

  @Override
  public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewholder, int position) {
    Launch launch = mLaunches.get(position);

    HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewholder;
    headerViewHolder.setData(String.valueOf(launch.launchYear));
  }

  private class LaunchViewHolder extends RecyclerView.ViewHolder {

    private final ImageView mMissionPhoto;
    private final TextView mMissionName;
    private final TextView mLaunchDate;
    private final TextView mSuccessful;

    LaunchViewHolder(View itemView) {
      super(itemView);

      mMissionPhoto = itemView.findViewById(R.id.iv_mission_patch);
      mMissionName = itemView.findViewById(R.id.tv_mission_name);
      mLaunchDate = itemView.findViewById(R.id.tv_launch_date);
      mSuccessful = itemView.findViewById(R.id.tv_successful);
    }

    void setData(final Launch launch) {

      if (launch.links.missionPatchSmall != null && !launch.links.missionPatchSmall.isEmpty()) {
        Picasso.get()
            .load(launch.links.missionPatchSmall)
            .transform(new CropSquareTransformation())
            .into(mMissionPhoto);
      }

      mMissionName.setText(launch.missionName);
      mLaunchDate.setText(launch.launchDateUtc);
      mSuccessful.setText(launch.launchSuccess);
    }
  }

  private class HeaderViewHolder extends RecyclerView.ViewHolder {

    private final TextView mYear;

    HeaderViewHolder(View itemView) {
      super(itemView);

      mYear = itemView.findViewById(R.id.tv_year);
    }

    void setData(final String year) {
      mYear.setText(year);
    }
  }
}