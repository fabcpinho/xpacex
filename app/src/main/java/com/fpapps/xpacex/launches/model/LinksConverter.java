package com.fpapps.xpacex.launches.model;

import android.arch.persistence.room.TypeConverter;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

public class LinksConverter {

  @TypeConverter
  public static Links fromString(String links) {
    Gson gson = new Gson();
    Type type = new TypeToken<Links>() {}.getType();
    return gson.fromJson(links, type);
  }

  @TypeConverter
  public static String toString(Links links) {
    Gson gson = new Gson();
    String str = gson.toJson(links);

    if (TextUtils.isEmpty(str) || str.equals("null")) {
      return "";
    }

    return str;
  }

}

