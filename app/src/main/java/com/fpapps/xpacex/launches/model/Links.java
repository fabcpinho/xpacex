package com.fpapps.xpacex.launches.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "links")
public class Links {
  @PrimaryKey(autoGenerate = true)
  public int id;

  @JsonProperty("mission_patch_small")
  @ColumnInfo(name = "missionPatchSmall")
  public String missionPatchSmall;
}
