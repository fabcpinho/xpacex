package com.fpapps.xpacex.launches.model;

import android.arch.persistence.room.TypeConverter;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

public class RocketIdConverter {

  @TypeConverter
  public static RocketId fromString(String rocket) {
    Gson gson = new Gson();
    Type type = new TypeToken<RocketId>() {}.getType();
    return gson.fromJson(rocket, type);
  }

  @TypeConverter
  public static String toString(RocketId rocket) {
    Gson gson = new Gson();
    String str = gson.toJson(rocket);

    if (TextUtils.isEmpty(str) || str.equals("null")) {
      return "";
    }

    return str;
  }

}

