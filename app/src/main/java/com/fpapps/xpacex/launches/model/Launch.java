package com.fpapps.xpacex.launches.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@TypeConverters({
                    LinksConverter.class,
                    RocketIdConverter.class
                })
@Entity(tableName = "launch")
public class Launch {
  @PrimaryKey
  @JsonProperty("flight_number")
  @ColumnInfo(name = "flightNumber")
  public int flightNumber;
  @JsonProperty("mission_name")
  @ColumnInfo(name = "missionName")
  public String missionName;
  @JsonProperty("launch_year")
  @ColumnInfo(name = "launchYear")
  public int launchYear;
  @JsonProperty("launch_date_utc")
  @ColumnInfo(name = "launchDateUtc")
  public String launchDateUtc;
  @JsonProperty("launch_success")
  @ColumnInfo(name = "launchSuccess")
  public String launchSuccess;
  @JsonProperty("rocket")
  @Embedded
  public RocketId rocket;
  @JsonProperty("links")
  @ColumnInfo(name = "links")
  public Links links;
}
