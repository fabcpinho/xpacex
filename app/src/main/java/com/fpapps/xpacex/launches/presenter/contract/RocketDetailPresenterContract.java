package com.fpapps.xpacex.launches.presenter.contract;

import com.fpapps.xpacex.launches.model.Launch;
import com.jjoe64.graphview.series.DataPoint;
import java.util.List;

public interface RocketDetailPresenterContract {

  /**
   * Gets the information of the rocked passed as parameter
   * @param id rocket id to fetch information for
   */
  void getRocketDetailedInformation(String id);

  /**
   * Failed fetching info
   * @param errorCode error code
   */
  void failedFetchingLaunches(int errorCode);

  /**
   * Launches are returned from the interactor and this method updates the view.
   * @param launches list of launches to display
   * @param dataPoints list of points to display in the graph
   */
  void details(List<Launch> launches, DataPoint[] dataPoints);

  /**
   * View is disposing
   */
  void dispose();
}
