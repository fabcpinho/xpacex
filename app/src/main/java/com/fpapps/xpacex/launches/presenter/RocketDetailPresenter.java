package com.fpapps.xpacex.launches.presenter;

import com.fpapps.xpacex.launches.view.contract.RocketDetailActivityContract;
import com.fpapps.xpacex.launches.interactor.LaunchesInteractor;
import com.fpapps.xpacex.launches.model.Launch;
import com.fpapps.xpacex.launches.presenter.contract.RocketDetailPresenterContract;
import com.fpapps.xpacex.rockets.repository.RocketsRepository;
import com.fpapps.xpacex.rxjava.RxSchedulers;
import com.jjoe64.graphview.series.DataPoint;
import java.util.List;

public class RocketDetailPresenter implements RocketDetailPresenterContract {
  private final LaunchesInteractor launchesInteractor;
  private RocketDetailActivityContract rocketDetailActivityContract;

  public RocketDetailPresenter(RxSchedulers rxSchedulers, RocketsRepository rocketsRepository,
                               RocketDetailActivityContract rocketDetailActivityContract) {
    this.rocketDetailActivityContract = rocketDetailActivityContract;
    launchesInteractor = new LaunchesInteractor(rxSchedulers, this, rocketsRepository);
  }

  @Override
  public void getRocketDetailedInformation(String id) {
    launchesInteractor.getRocketDetailedInformation(id);
  }

  @Override
  public void failedFetchingLaunches(int errorCode) {
    rocketDetailActivityContract.showError(errorCode);
  }

  @Override
  public void details(List<Launch> launches, DataPoint[] dataPoints) {
    if (launches.size() > 0) {
      rocketDetailActivityContract.displayListOfLaunches(launches);
    } else {
      rocketDetailActivityContract.showEmptyListPlaceholder();
    }

    rocketDetailActivityContract.displayGraph(dataPoints);
  }

  @Override
  public void dispose() {
    launchesInteractor.dispose();
  }
}
