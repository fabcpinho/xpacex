package com.fpapps.xpacex.api;

import android.support.annotation.NonNull;
import com.fpapps.xpacex.launches.api.LaunchesApiContract;
import com.fpapps.xpacex.rockets.api.RocketsApiContract;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitHelper {

    /**
     * This method configures a retrofit instance of RocketsApiContract.
     *
     * @param retrofit A retrofit instance
     * @return a RealEstateApiService instance
     */
    public RocketsApiContract makeARestRocketsService(Retrofit retrofit) {
        if (retrofit != null) {
            return retrofit.create(RocketsApiContract.class);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * This method configures a retrofit instance of LaunchesApiContract.
     *
     * @param retrofit A retrofit instance
     * @return a RealEstateApiService instance
     */
    public LaunchesApiContract makeARestLaunchesService(Retrofit retrofit) {
        if (retrofit != null) {
            return retrofit.create(LaunchesApiContract.class);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Configures the Retrofit builder
     *
     * @param formattedUrl an url already formatted (by getBaseUrl)
     * @return a configured retrofit instance
     */
    public Retrofit configureRetrofitBuilder(@NonNull String formattedUrl) {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(formattedUrl);
        builder.addConverterFactory(JacksonConverterFactory.create());
        builder.addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        return builder.build();
    }
}
