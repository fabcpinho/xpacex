package com.fpapps.xpacex.rxjava;

import io.reactivex.FlowableTransformer;
import io.reactivex.MaybeTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RxSchedulers {

    private final SingleTransformer singleSchedulersTransformer = upstream -> upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

    private final MaybeTransformer maybeSchedulersTransformer = upstream -> upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

    private final ObservableTransformer observableSchedulersTransformer =
            upstream -> upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

    private final FlowableTransformer flowableSchedulersTransformer = upstream -> upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

    @SuppressWarnings("unchecked")
    public <T> SingleTransformer<T, T> applySingleSchedulerTransformer() {
        return (SingleTransformer<T, T>) singleSchedulersTransformer;
    }

    @SuppressWarnings("unchecked")
    public <T> MaybeTransformer<T, T> applyMaybeSchedulerTransformer() {
        return (MaybeTransformer<T, T>) maybeSchedulersTransformer;
    }

    @SuppressWarnings("unchecked")
    public <T> ObservableTransformer<T, T> applyObservableSchedulerTransformer() {
        return (ObservableTransformer<T, T>) observableSchedulersTransformer;
    }

    @SuppressWarnings("unchecked")
    public <T> FlowableTransformer<T, T> applyFlowableSchedulerTransformer() {
        return (FlowableTransformer<T, T>) flowableSchedulersTransformer;
    }
}
