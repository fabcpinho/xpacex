package com.fpapps.xpacex.rockets.repository;

import com.fpapps.xpacex.database.ApplicationDatabase;
import com.fpapps.xpacex.database.LaunchDao;
import com.fpapps.xpacex.database.RocketDao;
import com.fpapps.xpacex.launches.api.LaunchesApiContract;
import com.fpapps.xpacex.launches.model.Launch;
import com.fpapps.xpacex.rockets.api.RocketsApiContract;
import com.fpapps.xpacex.rockets.model.Rocket;
import io.reactivex.Single;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Response;

// TODO: 09/10/2018 separate this into different repositories to access database and network layers
public class RocketsRepository {
  public final static int ERROR_LOADING_FROM_NETWORK = 0;
  public final static int ERROR_LOADING_FROM_DATABASE = 1;
  public final static int ERROR_LOADING_FROM_CACHE = 2;

  private final LaunchDao launchesDao;
  private LaunchesApiContract launchesApiContract;

  private RocketDao rocketDao;
  private RocketsApiContract rocketsApiContract;

  private List<Rocket> cacheRockets;

  public RocketsRepository(ApplicationDatabase applicationDatabase,
                           RocketsApiContract rocketsApiContract,
                           LaunchesApiContract launchesApiContract) {
    this.rocketsApiContract = rocketsApiContract;
    this.launchesApiContract = launchesApiContract;
    rocketDao = applicationDatabase.rocketDao();
    launchesDao = applicationDatabase.launchDao();
    initializeCache();
  }

  private void initializeCache() {
    cacheRockets = new ArrayList<>();
  }

  public Single<Response<List<Rocket>>> getNetworkRockets() {
    return rocketsApiContract.getRockets();
  }

  public Single<List<Rocket>> getDatabaseRockets() {
    return rocketDao.getRockets();
  }

  public List<Rocket> getCachedRockets() {
    return cacheRockets;
  }

  public List<Rocket> getActiveCachedRockets() {
    List<Rocket> activeRockets = new ArrayList<>();
    for (Rocket rocket : cacheRockets) {
      if (rocket.active) activeRockets.add(rocket);
    }
    return activeRockets;
  }

  public void addToCache(List<Rocket> rocketList) {
    cacheRockets.addAll(rocketList);
  }

  public void insertRocketsInDatabase(List<Rocket> rocketList) {
    rocketDao.insertRockets(rocketList);
  }

  // ---------------------------------------- LAUNCHES --------------------------------------

  public Single<Response<List<Launch>>> getRocketLaunchInfo(String id) {
    return launchesApiContract.getLaunchesByRocket(id);
  }

  public void insertLaunchesInDatabase(List<Launch> launches) {
    launchesDao.insertLaunches(launches);
  }

  public Single<List<Launch>> getLaunchesByRocket(String id) {
    return launchesDao.getLaunchesByRocket(id);
  }

  public Single<List<Launch>> getLaunches() {
    return launchesDao.getLaunches();
  }
}
