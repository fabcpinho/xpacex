package com.fpapps.xpacex.rockets.view.contract;

import com.fpapps.xpacex.rockets.model.Rocket;
import java.util.List;

public interface RocketsActivityContract {
  /**
   * Displays rockets in the recycler view
   * @param rocketList to display
   */
  void displayRockets(List<Rocket> rocketList);

  /**
   * Show error message
   * @param error can be Network or Database
   */
  void showErrorMessage(int error);
}
