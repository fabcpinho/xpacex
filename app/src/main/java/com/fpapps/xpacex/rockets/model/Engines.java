package com.fpapps.xpacex.rockets.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "engines")
public class Engines {
  @PrimaryKey
  @JsonProperty("number")
  @ColumnInfo(name = "number")
  public int number;
}
