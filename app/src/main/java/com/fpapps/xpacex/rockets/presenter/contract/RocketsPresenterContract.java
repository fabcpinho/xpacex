package com.fpapps.xpacex.rockets.presenter.contract;

import com.fpapps.xpacex.rockets.model.Rocket;
import java.util.List;

public interface RocketsPresenterContract {
  /**
   * Rockets are returned from the interactor and this method updates the view.
   * @param rockets list of rockets loaded from DB, Cache, or Network
   */
  void rockets(List<Rocket> rockets);

  /**
   * Handles error on loading
   * @param errorLoadingFromDatabase error code
   */
  void failedFetchingRockets(int errorLoadingFromDatabase);

  /**
   * Requests the list of rockets to the interactor
   */
  void getAllRockets();

  /**
   * Requests only the active rockets to the interactor
   */
  void getActiveRockets();

  /**
   * View is disposing
   */
  void dispose();
}
