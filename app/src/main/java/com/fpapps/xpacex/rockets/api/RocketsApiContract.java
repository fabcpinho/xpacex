package com.fpapps.xpacex.rockets.api;

import com.fpapps.xpacex.rockets.model.Rocket;
import io.reactivex.Single;
import java.util.List;
import retrofit2.Response;
import retrofit2.http.GET;

public interface RocketsApiContract {
  @GET("rockets")
  Single<Response<List<Rocket>>> getRockets();
}
