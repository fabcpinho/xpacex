package com.fpapps.xpacex.rockets.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.fpapps.xpacex.R;
import com.fpapps.xpacex.rockets.model.Rocket;
import com.squareup.picasso.Picasso;
import java.util.List;
import jp.wasabeef.picasso.transformations.CropSquareTransformation;

public class RocketsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private List<Rocket> mRockets;
  private View.OnClickListener clickListener;
  private String engines;

  public RocketsRecyclerViewAdapter(List<Rocket> mRockets, View.OnClickListener clickListener) {
    this.mRockets = mRockets;
    this.clickListener = clickListener;
  }

  public void setData(List<Rocket> mRockets) {
    this.mRockets = mRockets;
  }

  public Rocket getRocketInPosition(int pos) {
    return mRockets.get(pos);
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View v = inflater.inflate(R.layout.rv_rocket, parent, false);
    v.setOnClickListener(clickListener);
    engines = parent.getContext().getString(R.string.engines);
    return new RocketViewHolder(v);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    Rocket rocket = mRockets.get(position);
    RocketViewHolder rocketViewHolder = (RocketViewHolder) holder;
    rocketViewHolder.setData(rocket);
  }

  @Override
  public int getItemCount() {
    return mRockets != null ? mRockets.size() : 0;
  }

  private class RocketViewHolder extends RecyclerView.ViewHolder {

    private final ImageView mRocketPhoto;
    private final TextView mName;
    private final TextView mCountry;
    private final TextView mEnginesCount;

    RocketViewHolder(View itemView) {
      super(itemView);

      mRocketPhoto = itemView.findViewById(R.id.iv_rocket_photo);
      mName = itemView.findViewById(R.id.tv_name);
      mCountry = itemView.findViewById(R.id.tv_country);
      mEnginesCount = itemView.findViewById(R.id.tv_engines_count);
    }

    void setData(final Rocket rocket) {

      // This images are heavy, should be replaced by lightweight images in order to provide good performance
      if (rocket.images.size() > 0) {
        Picasso.get()
            .load(rocket.images.get(0))
            .transform(new CropSquareTransformation())
            .into(mRocketPhoto);
      }

      mName.setText(rocket.rocketName);
      mCountry.setText(rocket.country);

      String enginesCount = String.valueOf(rocket.engines.number) + " " + engines;
      mEnginesCount.setText(enginesCount);
    }
  }
}