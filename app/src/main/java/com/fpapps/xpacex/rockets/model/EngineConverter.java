package com.fpapps.xpacex.rockets.model;

import android.arch.persistence.room.TypeConverter;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

public class EngineConverter {

  @TypeConverter
  public static Engines fromString(String engines) {
    Gson gson = new Gson();
    Type type = new TypeToken<Engines>() {}.getType();
    return gson.fromJson(engines, type);
  }

  @TypeConverter
  public static String toString(Engines engines) {
    Gson gson = new Gson();
    String str = gson.toJson(engines);

    if (TextUtils.isEmpty(str) || str.equals("null")) {
      return "";
    }

    return str;
  }

}

