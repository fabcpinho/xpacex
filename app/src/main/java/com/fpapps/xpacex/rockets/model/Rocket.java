package com.fpapps.xpacex.rockets.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
@TypeConverters({
                    EngineConverter.class,
                    ArrayConverter.class
                })
@Entity(tableName = "rocket")
public class Rocket {
  @PrimaryKey
  @JsonProperty("rocket_id")
  @ColumnInfo(name = "rocketId")
  @NonNull
  public String rocketId;
  @JsonProperty("active")
  @ColumnInfo(name = "active")
  public Boolean active;
  @JsonProperty("rocket_name")
  @ColumnInfo(name = "rocketName")
  public String rocketName;
  @JsonProperty("country")
  @ColumnInfo(name = "country")
  public String country;
  @JsonProperty("engines")
  @ColumnInfo(name = "engines")
  public Engines engines;
  @JsonProperty("flickr_images")
  @ColumnInfo(name = "images")
  public ArrayList<String> images;
  @JsonProperty("description")
  @ColumnInfo(name = "description")
  public String description;

}
