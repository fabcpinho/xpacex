package com.fpapps.xpacex.rockets.presenter;

import com.fpapps.xpacex.rockets.interactor.RocketsInteractor;
import com.fpapps.xpacex.rockets.model.Rocket;
import com.fpapps.xpacex.rockets.presenter.contract.RocketsPresenterContract;
import com.fpapps.xpacex.rockets.repository.RocketsRepository;
import com.fpapps.xpacex.rockets.view.contract.RocketsActivityContract;
import com.fpapps.xpacex.rxjava.RxSchedulers;
import java.util.List;

public class RocketsPresenter implements RocketsPresenterContract {

  private RocketsActivityContract rocketsActivityContract;
  private RocketsInteractor rocketsInteractor;

  public RocketsPresenter(RocketsActivityContract rocketsActivityContract,
                          RxSchedulers rxSchedulers, RocketsRepository rocketsRepository) {
    this.rocketsActivityContract = rocketsActivityContract;
    this.rocketsInteractor = new RocketsInteractor(rxSchedulers, this, rocketsRepository);
  }

  @Override
  public void rockets(List<Rocket> rockets) {
    rocketsActivityContract.displayRockets(rockets);
  }

  @Override
  public void failedFetchingRockets(int errorLoadingFromDatabase) {
    rocketsActivityContract.showErrorMessage(errorLoadingFromDatabase);
  }

  @Override
  public void getAllRockets() {
    rocketsInteractor.getRockets();
  }

  @Override
  public void getActiveRockets() {
    rocketsInteractor.getActiveRockets();
  }

  @Override
  public void dispose() {
    rocketsInteractor.dispose();
  }
}
