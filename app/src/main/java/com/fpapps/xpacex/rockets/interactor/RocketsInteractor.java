package com.fpapps.xpacex.rockets.interactor;

import com.fpapps.xpacex.rockets.model.Rocket;
import com.fpapps.xpacex.rockets.presenter.contract.RocketsPresenterContract;
import com.fpapps.xpacex.rockets.repository.RocketsRepository;
import com.fpapps.xpacex.rxjava.RxSchedulers;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import retrofit2.Response;

public class RocketsInteractor {

  private final RxSchedulers rxSchedulers;
  private RocketsPresenterContract rocketsPresenterContract;
  private RocketsRepository rocketsRepository;

  private final CompositeDisposable mDisposables;

  public RocketsInteractor(RxSchedulers rxSchedulers,
                           RocketsPresenterContract rocketsPresenterContract,
                           RocketsRepository rocketsRepository) {
    this.rxSchedulers = rxSchedulers;
    this.rocketsPresenterContract = rocketsPresenterContract;
    this.rocketsRepository = rocketsRepository;

    mDisposables = new CompositeDisposable();
  }

  private void fetchDataFromApi() {
    mDisposables.add(fetchRocketsFromApi().subscribe(rockets -> {
      // Persist rockets in database
      updateDatabase(rockets);
      // Updates cache
      updateCache(rockets);
      // Try to get rockets again
      getRockets();
    }, throwable -> rocketsPresenterContract.failedFetchingRockets(
        RocketsRepository.ERROR_LOADING_FROM_NETWORK)));
  }

  private void updateCache(List<Rocket> rockets) {
    rocketsRepository.addToCache(rockets);
  }

  private Maybe<List<Rocket>> fetchRocketsFromApi() {
    return rocketsRepository.getNetworkRockets()
        .map(Response::body)
        .filter(response -> response != null)
        .compose(rxSchedulers.applyMaybeSchedulerTransformer());
  }

  private void updateDatabase(List<Rocket> rocketList) {
    mDisposables.add(Single.just(rocketList)
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .subscribe(request -> rocketsRepository.insertRocketsInDatabase(rocketList)));
  }

  /**
   * If we have rockets in cache, load them.
   * If no rockets in cache, check database.
   * If database is empty try to load data from API.
   */
  public void getRockets() {
    if (rocketsRepository.getCachedRockets().size() > 0) {
      rocketsPresenterContract.rockets(rocketsRepository.getCachedRockets());
      return;
    }

    mDisposables.add(rocketsRepository.getDatabaseRockets()
        .map(rockets -> {
          // If number of rockets in database is 0, fetch from api
          if (rockets.size() == 0) {
            fetchDataFromApi();
            return null;
          }

          return rockets;
        })
        .filter(rockets -> rockets != null && rockets.size() != 0)
        .compose(rxSchedulers.applyMaybeSchedulerTransformer())
        .subscribe(rockets -> {
          //successfully loaded rockets from database into cache
          if (rockets != null) {
            updateCache(rockets);
            rocketsPresenterContract.rockets(rockets);
          }
        }, throwable -> rocketsPresenterContract.failedFetchingRockets(
            RocketsRepository.ERROR_LOADING_FROM_DATABASE)));
  }

  public void getActiveRockets() {
    if (rocketsRepository.getActiveCachedRockets().size() > 0) {
      rocketsPresenterContract.rockets(rocketsRepository.getActiveCachedRockets());
    } else {
      rocketsPresenterContract.failedFetchingRockets(RocketsRepository.ERROR_LOADING_FROM_CACHE);
    }
  }

  /**
   * Dispose all observables
   */
  public void dispose() {
    mDisposables.dispose();
  }
}
