package com.fpapps.xpacex.rockets.view;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import com.fpapps.xpacex.R;
import com.fpapps.xpacex.dagger.component.DaggerRocketsActivityComponent;
import com.fpapps.xpacex.dagger.module.ApplicationModule;
import com.fpapps.xpacex.dagger.module.RocketsPresenterModule;
import com.fpapps.xpacex.launches.view.RocketDetailActivity;
import com.fpapps.xpacex.rockets.adapters.RocketsRecyclerViewAdapter;
import com.fpapps.xpacex.rockets.model.Rocket;
import com.fpapps.xpacex.rockets.presenter.contract.RocketsPresenterContract;
import com.fpapps.xpacex.rockets.repository.RocketsRepository;
import com.fpapps.xpacex.rockets.view.contract.RocketsActivityContract;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import static com.fpapps.xpacex.launches.view.RocketDetailActivity.ROCKET_DESCRIPTION_BUNDLE;
import static com.fpapps.xpacex.launches.view.RocketDetailActivity.ROCKET_ID_BUNDLE;

public class RocketsActivity extends AppCompatActivity
    implements RocketsActivityContract, View.OnClickListener {

  @Inject
  RocketsPresenterContract rocketsPresenter;
  private RocketsRecyclerViewAdapter mAdapter;
  private RecyclerView mRecyclerView;
  private Toolbar toolbar;
  private ProgressBar progressBar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_rockets);
    handleDaggerInjection();

    toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    toolbar.inflateMenu(R.menu.toolbar_menu);

    progressBar = findViewById(R.id.pb_loading);

    setUpRecyclerView();

    getRockets();
  }

  private void getRockets() {
    rocketsPresenter.getAllRockets();
    progressBar.setIndeterminate(true);
  }

  private void setUpRecyclerView() {
    mRecyclerView = findViewById(R.id.rv_rockets);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
    mRecyclerView.setLayoutManager(mLayoutManager);

    mAdapter = new RocketsRecyclerViewAdapter(new ArrayList<>(), this);
    mRecyclerView.setAdapter(mAdapter);
  }

  private void handleDaggerInjection() {
    DaggerRocketsActivityComponent.builder()
        .applicationModule(new ApplicationModule(this))
        .rocketsPresenterModule(new RocketsPresenterModule(this))
        .build()
        .inject(this);
  }

  @Override
  public void displayRockets(List<Rocket> rocketList) {
    mAdapter.setData(rocketList);
    mAdapter.notifyDataSetChanged();

    progressBar.setVisibility(View.GONE);
    progressBar.setIndeterminate(false);
  }

  @Override
  public void showErrorMessage(int errorType) {
    progressBar.setVisibility(View.GONE);
    progressBar.setIndeterminate(false);

    if (errorType == RocketsRepository.ERROR_LOADING_FROM_NETWORK) {
      new AlertDialog.Builder(this).setTitle(R.string.no_internet_tittle)
          .setMessage(R.string.no_internet_message)
          .setCancelable(false)
          .setPositiveButton(getString(R.string.try_again), (dialogInterface, i) -> {
            getRockets();
            dialogInterface.dismiss();
          })
          .show();
    }
  }

  @Override
  public void onClick(View view) {
    int itemPosition = mRecyclerView.getChildLayoutPosition(view);
    Rocket mRocket = mAdapter.getRocketInPosition(itemPosition);

    Intent intent = new Intent(this, RocketDetailActivity.class);
    intent.putExtra(ROCKET_ID_BUNDLE, mRocket.rocketId);
    intent.putExtra(ROCKET_DESCRIPTION_BUNDLE, mRocket.description);
    startActivity(intent);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.toolbar_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection
    switch (item.getItemId()) {
      case R.id.menu_active:
        rocketsPresenter.getActiveRockets();
        return true;
      case R.id.menu_all:
        rocketsPresenter.getAllRockets();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    rocketsPresenter.dispose();
  }
}
