package com.fpapps.xpacex.dagger.module;

import com.fpapps.xpacex.api.RetrofitHelper;
import com.fpapps.xpacex.launches.api.LaunchesApiContract;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import retrofit2.Retrofit;

@Module(includes = {
    RetrofitHelperModule.class,
    RetrofitModule.class
})
public class LaunchesApiContractModule {
  @Provides
  @Singleton
  LaunchesApiContract getRocketsApiContract(RetrofitHelper retrofitHelper, Retrofit retrofit) {
    return retrofitHelper.makeARestLaunchesService(retrofit);
  }
}
