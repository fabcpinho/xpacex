package com.fpapps.xpacex.dagger.module;

import com.fpapps.xpacex.database.ApplicationDatabase;
import com.fpapps.xpacex.launches.api.LaunchesApiContract;
import com.fpapps.xpacex.rockets.api.RocketsApiContract;
import com.fpapps.xpacex.rockets.repository.RocketsRepository;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module(includes = {
    ApplicationDatabaseModule.class,
    RocketsApiContractModule.class,
    LaunchesApiContractModule.class
})
public class RocketRepositoryModule {
  @Provides
  @Singleton
  public RocketsRepository providesRequestRepository(ApplicationDatabase applicationDatabase,
                                                     RocketsApiContract messagesApiContract,
                                                     LaunchesApiContract launchesApiContract) {
    return new RocketsRepository(applicationDatabase, messagesApiContract, launchesApiContract);
  }
}
