package com.fpapps.xpacex.dagger.module;

import android.content.Context;
import com.fpapps.xpacex.database.ApplicationDatabase;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module(includes = ApplicationModule.class)
public class ApplicationDatabaseModule {
    @Provides
    @Singleton
    ApplicationDatabase getApplicationDatabase(Context context){
        return ApplicationDatabase.getAppDatabase(context);
    }
}
