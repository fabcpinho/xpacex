package com.fpapps.xpacex.dagger.module;

import com.fpapps.xpacex.rockets.presenter.RocketsPresenter;
import com.fpapps.xpacex.rockets.presenter.contract.RocketsPresenterContract;
import com.fpapps.xpacex.rockets.repository.RocketsRepository;
import com.fpapps.xpacex.rockets.view.contract.RocketsActivityContract;
import com.fpapps.xpacex.rxjava.RxSchedulers;
import dagger.Module;
import dagger.Provides;

@Module(includes = {
    RocketRepositoryModule.class,
    RxSchedulersModule.class
})
public class RocketsPresenterModule {

  private RocketsActivityContract rocketsActivityContract;

  public RocketsPresenterModule(RocketsActivityContract rocketsActivityContract) {
    this.rocketsActivityContract = rocketsActivityContract;
  }

  @Provides
  RocketsPresenterContract providesRocketsPresenter(RxSchedulers rxSchedulers,
                                                    RocketsRepository rocketsRepository) {
    return new RocketsPresenter(rocketsActivityContract, rxSchedulers, rocketsRepository);
  }
}
