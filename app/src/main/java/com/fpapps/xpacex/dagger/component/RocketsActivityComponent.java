package com.fpapps.xpacex.dagger.component;

import com.fpapps.xpacex.dagger.module.RocketsPresenterModule;
import com.fpapps.xpacex.rockets.view.RocketsActivity;
import dagger.Component;
import javax.inject.Singleton;

@Singleton
@Component(modules = {
    RocketsPresenterModule.class
})
public interface RocketsActivityComponent {
  void inject(RocketsActivity rocketsActivity);
}
