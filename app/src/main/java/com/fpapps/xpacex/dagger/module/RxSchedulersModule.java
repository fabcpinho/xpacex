package com.fpapps.xpacex.dagger.module;

import com.fpapps.xpacex.rxjava.RxSchedulers;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module
public class RxSchedulersModule {
  @Provides
  @Singleton
  public RxSchedulers providesRxSchedulers() {
    return new RxSchedulers();
  }
}
