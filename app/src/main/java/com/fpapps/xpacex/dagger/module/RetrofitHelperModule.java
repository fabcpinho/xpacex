package com.fpapps.xpacex.dagger.module;

import com.fpapps.xpacex.api.RetrofitHelper;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module
public class RetrofitHelperModule {

  @Singleton
  @Provides
  public RetrofitHelper retrofitHelper(){
    return new RetrofitHelper();
  }
}
