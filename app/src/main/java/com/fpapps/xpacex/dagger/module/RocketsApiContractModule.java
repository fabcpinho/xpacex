package com.fpapps.xpacex.dagger.module;

import com.fpapps.xpacex.api.RetrofitHelper;
import com.fpapps.xpacex.rockets.api.RocketsApiContract;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import retrofit2.Retrofit;

@Module(includes = {
    RetrofitHelperModule.class,
    RetrofitModule.class
})
public class RocketsApiContractModule {
  @Provides
  @Singleton
  RocketsApiContract getRocketsApiContract(RetrofitHelper retrofitHelper, Retrofit retrofit) {
    return retrofitHelper.makeARestRocketsService(retrofit);
  }
}
