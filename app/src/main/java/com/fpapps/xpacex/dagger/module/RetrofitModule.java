package com.fpapps.xpacex.dagger.module;

import com.fpapps.xpacex.api.RetrofitHelper;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import retrofit2.Retrofit;

@Module(includes = RetrofitHelperModule.class)
public class RetrofitModule {
    private String url = "https://api.spacexdata.com/v3/";

    @Provides
    @Singleton
    public Retrofit providesRetrofit(RetrofitHelper retrofitHelper) {
        return retrofitHelper.configureRetrofitBuilder(url);
    }
}
