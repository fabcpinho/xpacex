package com.fpapps.xpacex.dagger.module;

import com.fpapps.xpacex.launches.view.contract.RocketDetailActivityContract;
import com.fpapps.xpacex.launches.presenter.RocketDetailPresenter;
import com.fpapps.xpacex.launches.presenter.contract.RocketDetailPresenterContract;
import com.fpapps.xpacex.rockets.repository.RocketsRepository;
import com.fpapps.xpacex.rxjava.RxSchedulers;
import dagger.Module;
import dagger.Provides;

@Module(includes = {
    RocketRepositoryModule.class,
    RxSchedulersModule.class
})
public class RocketDetailPresenterModule {

  private RocketDetailActivityContract rocketDetailActivityContract;

  public RocketDetailPresenterModule(RocketDetailActivityContract rocketDetailActivityContract) {
    this.rocketDetailActivityContract = rocketDetailActivityContract;
  }

  @Provides
  RocketDetailPresenterContract providesRocketDetailPresenter(RxSchedulers rxSchedulers,
                                                         RocketsRepository rocketsRepository) {
    return new RocketDetailPresenter(rxSchedulers, rocketsRepository, rocketDetailActivityContract);
  }
}
