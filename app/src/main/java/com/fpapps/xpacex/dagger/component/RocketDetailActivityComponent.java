package com.fpapps.xpacex.dagger.component;

import com.fpapps.xpacex.dagger.module.RocketDetailPresenterModule;
import com.fpapps.xpacex.launches.view.RocketDetailActivity;
import dagger.Component;
import javax.inject.Singleton;

@Singleton
@Component(modules = {
    RocketDetailPresenterModule.class
})
public interface RocketDetailActivityComponent {
  void inject(RocketDetailActivity rocketDetailActivity);
}
