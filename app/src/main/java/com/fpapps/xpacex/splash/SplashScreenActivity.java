package com.fpapps.xpacex.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.fpapps.xpacex.R;
import com.fpapps.xpacex.rockets.view.RocketsActivity;
import com.squareup.picasso.Picasso;

public class SplashScreenActivity extends AppCompatActivity {

  private boolean animationEnded = false;
  private ImageView imageView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);

    imageView = findViewById(R.id.rocketIv);
    Picasso.get().load(R.drawable.rocket).into(imageView);

    startAnimations();
  }

  private void startAnimations() {
    Animation anim = AnimationUtils.loadAnimation(this, R.anim.splash_screen_animation);
    anim.reset();
    LinearLayout l = findViewById(R.id.lin_lay);
    l.clearAnimation();
    l.startAnimation(anim);

    anim = AnimationUtils.loadAnimation(this, R.anim.splash_screen_translation);
    anim.reset();

    imageView.setVisibility(View.VISIBLE);
    imageView.clearAnimation();
    imageView.startAnimation(anim);

    imageView.getAnimation().setAnimationListener(getSplashAnimationListener());
  }

  @NonNull
  private Animation.AnimationListener getSplashAnimationListener() {
    return new Animation.AnimationListener() {
      @Override
      public void onAnimationStart(Animation animation) {
        // Do nothing
      }

      @Override
      public void onAnimationEnd(Animation animation) {
        animationEnded = true;
        startNextActivity();
      }

      @Override
      public void onAnimationRepeat(Animation animation) {
        // Do nothing
      }
    };
  }

  private void startNextActivity() {
    if (animationEnded) {
      Intent intent = new Intent(this, RocketsActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(intent);
      finish();
    }
  }
}
