package com.fpapps.xpacex.stickyheader;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.Map;

public class StickyHeaderDecoration extends RecyclerView.ItemDecoration {

    private static final int OFFSET = 20;

    private Map<Long, RecyclerView.ViewHolder> mHeaderCache;

    private StickyHeaderAdapter mAdapter;
    private OnHeaderViewRequestedListener mOnHeaderViewRequestedListener;

    /**
     * Interface definition for a callback to be invoked when a header view is requested to be drawn.
     */
    public interface OnHeaderViewRequestedListener {
        /**
         * Called when a header view has been requested to be drawn.
         *
         * @param viewHolder The view's holder that was requested to be drawn.
         * @param position The header's position.
         */
        void onHeaderViewRequested(RecyclerView.ViewHolder viewHolder, int position);
    }

    public StickyHeaderDecoration(StickyHeaderAdapter adapter, OnHeaderViewRequestedListener listener) {
        mAdapter = adapter;
        mHeaderCache = new HashMap<>();
        mOnHeaderViewRequestedListener = listener;
    }

    /**
     * @param adapter the sticky header adapter to use
     */
    public StickyHeaderDecoration(StickyHeaderAdapter adapter) {
        this(adapter, null);
        mAdapter = adapter;
        mHeaderCache = new HashMap<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);

        int headerHeight = 0;
        int bottomHeight = 0;
        if (hasHeader(position)) {
            View header = getHeader(parent, position).itemView;
            headerHeight = header.getHeight();

            if (position == 0)
                headerHeight = headerHeight + OFFSET;
        }
        if (isLastFromGroup(position, parent.getAdapter().getItemCount())){
            // Check if is last from group
            bottomHeight = bottomHeight + OFFSET;
        }

        outRect.set(0, headerHeight, 0, bottomHeight);
    }

    /**
     * Clears the header view cache. Headers will be recreated and
     * rebound on list scroll after this method has been called.
     */
    public void clearHeaderCache() {
        mHeaderCache.clear();
    }

    private boolean hasHeader(int position) {
        if (position == 0) {
            return true;
        }

        int previous = position - 1;
        return mAdapter.getHeaderId(position) != mAdapter.getHeaderId(previous);
    }

    private boolean isLastFromGroup(int position, int adapterSize) {
        if (position == adapterSize - 1) {
            return true;
        }

        int after = position + 1;
        return mAdapter.getHeaderId(position) != mAdapter.getHeaderId(after);
    }

    private RecyclerView.ViewHolder getHeader(RecyclerView parent, int position) {
        final long key = mAdapter.getHeaderId(position);

        if (mHeaderCache.containsKey(key)) {
            RecyclerView.ViewHolder holder = mHeaderCache.get(key);
            if(mOnHeaderViewRequestedListener != null) mOnHeaderViewRequestedListener.onHeaderViewRequested(holder, position);
            return holder;
        } else {
            final RecyclerView.ViewHolder holder = mAdapter.onCreateHeaderViewHolder(parent, position);
            final View header = holder.itemView;

            //noinspection unchecked
            mAdapter.onBindHeaderViewHolder(holder, position);

            int widthSpec = View.MeasureSpec.makeMeasureSpec(parent.getWidth(), View.MeasureSpec.EXACTLY);
            int heightSpec = View.MeasureSpec.makeMeasureSpec(parent.getHeight(), View.MeasureSpec.UNSPECIFIED);

            int childWidth = ViewGroup.getChildMeasureSpec(widthSpec,
                    parent.getPaddingLeft() + parent.getPaddingRight(), header.getLayoutParams().width);
            int childHeight = ViewGroup.getChildMeasureSpec(heightSpec,
                    parent.getPaddingTop() + parent.getPaddingBottom(), header.getLayoutParams().height);

            header.measure(childWidth, childHeight);
            header.layout(0, 0, header.getMeasuredWidth(), header.getMeasuredHeight());

            mHeaderCache.put(key, holder);
            if(mOnHeaderViewRequestedListener != null) mOnHeaderViewRequestedListener.onHeaderViewRequested(holder, position);
            return holder;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        final int count = parent.getChildCount();

        for (int layoutPos = 0; layoutPos < count; layoutPos++) {
            final View child = parent.getChildAt(layoutPos);

            if (child == null) return;

            final int adapterPos = parent.getChildAdapterPosition(child);

            if (adapterPos == -1) return;

            if (layoutPos == 0 || hasHeader(adapterPos)) {
                View header = getHeader(parent, adapterPos).itemView;
                c.save();
                final int left = child.getLeft();
                final int top = getHeaderTop(parent, child, header, adapterPos, layoutPos);
                c.translate(left, top);
                header.draw(c);
                c.restore();
            }
        }
    }

    private int getHeaderTop(RecyclerView parent, View child, View header, int adapterPos, int layoutPos) {
        int top = child.getTop() - header.getHeight();
        if (layoutPos == 0) {
            final int count = parent.getChildCount();
            final long currentId = mAdapter.getHeaderId(adapterPos);
            // find next view with header and compute the offscreen push if needed
            for (int i = 1; i < count - 1; i++) {
                long nextId = mAdapter.getHeaderId(adapterPos + i);
                if (nextId != currentId && nextId != -1) {
                    final View next = parent.getChildAt(i);
                    final int offset = next.getTop() - (header.getHeight() + getHeader(parent, i).itemView.getHeight());
                    if (offset < 0) {
                        return offset;
                    } else {
                        break;
                    }
                }
            }

            top = Math.max(0, top);
        }

        return top;
    }

}

