package com.fpapps.xpacex.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.fpapps.xpacex.rockets.model.Rocket;
import io.reactivex.Single;
import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface RocketDao {
    @Query("SELECT * FROM rocket")
    Single<List<Rocket>> getRockets();

    @Insert(onConflict = REPLACE)
    void insertRockets(List<Rocket> rockets);
}
