package com.fpapps.xpacex.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import com.fpapps.xpacex.launches.model.Launch;
import com.fpapps.xpacex.launches.model.Links;
import com.fpapps.xpacex.rockets.model.Engines;
import com.fpapps.xpacex.rockets.model.Rocket;

@Database(entities = {
    Rocket.class,
    Engines.class,
    Launch.class,
    Links.class
}, version = 2)
public abstract class ApplicationDatabase extends RoomDatabase {

  private static ApplicationDatabase mAppDatabase;

  public static ApplicationDatabase getAppDatabase(Context context) {
    if (mAppDatabase == null) {
      mAppDatabase = Room.databaseBuilder(context, ApplicationDatabase.class, "dbspacex").build();
    }

    return mAppDatabase;
  }

  public abstract LaunchDao launchDao();

  public abstract RocketDao rocketDao();
}