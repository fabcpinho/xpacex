package com.fpapps.xpacex.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.fpapps.xpacex.launches.model.Launch;
import io.reactivex.Single;
import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface LaunchDao {
  @Query("SELECT * FROM launch")
  Single<List<Launch>> getLaunches();

  @Query("SELECT * FROM launch WHERE id= :rocketId ORDER BY launchYear")
  Single<List<Launch>> getLaunchesByRocket(String rocketId);

  @Insert(onConflict = REPLACE)
  void insertLaunches(List<Launch> launches);
}
